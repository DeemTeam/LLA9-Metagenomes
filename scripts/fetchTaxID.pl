#!/usr/bin/perl -w

use strict ;

my $diamond = $ARGV[0] ;
my $DB_path = "/home/ana/LAB/databases/" ; ## <---- Change here for your path where your "DB" lives (ACCN_TaxID.tab)

unless ($diamond) {
	print "Diamond (with 12 fields tab separated) output: " ;
	chomp($diamond = <STDIN>) ;
}

my ($mat, $round) = (split/\./,$diamond)[0, 1] ;
my $output = "$mat"."."."$round"."_covTaxID.tab" ;
my $taxIDs = "$DB_path"."/ACCN_TaxID.tab" ; #my $localDB = "/home/ana/LAB/databases/ACCN_TaxID.tab" ;
my $errorf = "$mat"."."."$round"."_covTaxID.err" ;
my $actxid = "$mat"."."."$round"."_ACCN_taxID.tab" ;

open (IN, "$diamond")  || die "Cant open $diamond\n"  ;
open (OUT, ">$output") || die "Cant create $output\n" ;
open (ATI, ">$actxid") || die "Cant create $actxid\n" ;
#open (ERR, ">$errorf") || die "Cant create $errorf\n" ;

while (<IN>) {
	chomp ;
	my $line  = $_ ;
	my ($sseqid, $length, $qstart, $qend) = (split/\t/,$line)[1, 3, 6, 7] ;
	next if ($sseqid eq "*") ;
	my $AccN = "" ;
	if ($sseqid =~ /^gi\|/) {
		$AccN = (split/\|/,$sseqid)[3] ;
	} else {
		$AccN = $sseqid ;
	}
	my $taxID = "" ;
	my $qCov  = (($qend - $qstart)/$length)*100 ; 		### For Blastp
#	my $qCov  = ((($qend - $qstart)/3)*100)/$length ;  	### For blastx
	
	my $hit = `grep -e \"$AccN\\b\" $taxIDs` ;
	if ($hit) {
		chomp $hit ;
		#print "!$hit!" ; <STDIN> ;
		$taxID = (split/\t/,$hit)[1] ;
		#print "$line\t$qCov\t$taxID\n" ; <STDIN> ;
		print OUT "$line\t$qCov\t$taxID\n" ;
		print ATI "$AccN\t$taxID\n" ;
	} else {
		my $fetch = `curl -s \"https\:\/\/eutils\.ncbi\.nlm\.nih\.gov\/entrez\/eutils\/efetch\.fcgi\?db\=protein\&id\=$AccN\&rettype\=fasta\&retmode\=xml\" \| grep taxid` ;
		if ($fetch) {
			chomp ($fetch) ;
			if ($fetch =~ /taxid>(\d+)\</) {
				$taxID = $1 ;
				#print "$AccN -> $1\n" ; #<STDIN> ;
				#print "$line\t$qCov\t$taxID\n" ;
				print OUT "$line\t$qCov\t$taxID\n" ; #<STDIN> ;
				print ATI "$AccN\t$taxID\n" ;
			} else {
				print "WARNING: efetch command returned empty for $fetch!\n" ;  #<STDIN> ;
				print OUT "$line\t$qCov\tNA\n" ;
			}
		}
	}
}



### UPDATE LOCAL TAXID DB ####
print "Updating DB... Do not cancel\n" ;
my $copy = "$DB_path"."last_ACCN_TaxID.tab" ;
system("cp -v $taxIDs $copy") ; 
system("cat $actxid $copy | sort | uniq > $taxIDs") ;
my $backup = "$DB_path"."backup_ACCN_TaxID.tab" ;
system("cp -v $taxIDs $backup") ; 
print "...done!\n" ;





#curl -s "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id=XP_008880257.1&rettype=fasta&retmode=xml" 