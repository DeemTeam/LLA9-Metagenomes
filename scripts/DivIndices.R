library(vegan)
rm(list = ls())


setwd("D:/LAB/Llamara/Metagenomes/DiversityIndices/")
AbTabl <- read.table("LLA9-Abundance_PF00238.tab", header = TRUE, sep = "\t", row.names = 1, quote="")

# Use the argument quote = "" inside read.table.
# read.table("your_file", quote="", other.arguments)
# Explanation: Your data has a single quote on 59th line (( pyridoxamine 5'-phosphate oxidase (predicted)). Then there is another single quote, which complements the single quote on line 59, is on line 137 (5'-hydroxyl-kinase activity...). Everything within quote will be read as a single field of data, and quotes can include the newline character also. That's why you lose the lines in between. quote = "" disables quoting altogether.

Mats <- row.names(AbTabl)
nrow(AbTabl)

########################################
######## Diversity indices
#Parametros de diversidad: Shannon, Inversa de Simpson, equidad de Pielou y 
#el numero #Metagenome seria las variables alfanumericas que escogeriamos para hacer el analisis 
#(e.g. vegetacion, sitios #Variable "Tabla" debe tener el mismo numero de renglones que la variable "factor"
#Variable "factor" debe tener distinto nombre
Diversidad <- function(Tabla,Metagenome){
  require(vegan)
  SpecNum <-specnumber(Tabla) ## #rowSums(BCI > 0)# Species richness
  ShannonD <- diversity(Tabla)#Shannon entropy
  Pielou <- ShannonD/log(SpecNum)#Pielou's evenness
  Simp <- (diversity(Tabla, "simpson"))# Indice de dominacia de Simpson
  TablaF <- data.frame(Metagenome,SpecNum, ShannonD, Simp, Pielou)
  print("Diversity_Indices")
  print(TablaF)
}
Diversidad(AbTabl,Mats)


########################################
#Chao data
#Many species will always remain unseen or undetected in a
#collection of sample plots.
#The function uses some popular ways of estimating
#the number of these unseen species and adding them
#to the observed species richness (Palmer 1990, Colwell & Coddington 1994).
#########################################

ChaoF <- function(Tabla, Factor){
  require(vegan)
  require(ggplot2)
  require(reshape)
  Tabla <- data.frame(Tabla, row.names=Factor)
  Tabla
  UNO <- estimateR(Tabla)[1:2,]
  UNO1 <- melt(UNO)
  names(UNO1)[1] <- c("Var")
  names(UNO1)[2] <- c("Veg")
  LL <- ggplot(UNO1, aes(x = Veg, y = value, fill = Var))
  LL <- LL + geom_bar(position = "stack", stat = "identity")
  LL <- LL + facet_wrap( ~ Var)+ theme_bw()
  LL <- LL + theme(axis.text.x = element_text(angle = 45, hjust = 1))
  print(LL)
  print(t(UNO))
#  print(t(UNO[2,]))
  
}
#ls()


ChaoF(AbTabl, Mats)

