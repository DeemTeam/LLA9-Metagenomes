library(Nonpareil);

#setwd("/home/ana/LAB/Llamara/Metagenomes/Coverage/")
Nonpareil.curve('A1.nonpareil.npo');
Nonpareil.curve('B1.nonpareil.npo', new = F);
Nonpareil.curve('C1.nonpareil.npo', new = F);
Nonpareil.curve('C2.II.nonpareil.npo', new = F);
Nonpareil.curve('C2.III.nonpareil.npo', new = F);
Nonpareil.curve('C3.II.nonpareil.npo', new = F);
Nonpareil.curve('C3.III.nonpareil.npo', new = F);
Nonpareil.curve('D1.I.nonpareil.npo', new = F);
Nonpareil.curve('D1.III.nonpareil.npo', new = F);
Nonpareil.curve('D2.I.nonpareil.npo', new = F);
Nonpareil.curve('D2.III.nonpareil.npo', new = F);
