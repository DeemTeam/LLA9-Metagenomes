#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Llamara/Metagenomes/Annotation/KEGG" ;
my $numg = "metabolisms_number_genes.csv" ;
my $abun = "Figure2b_Abundance.csv" ;

my %mets ;

open (MET, "$path/$numg") || die "Cant open $path/$numg\n" ;
while (<MET>) {
	chomp ;
	my ($met, $num) = (split/\t/,$_) ;
	$mets{$met} = $num ;
}
close MET ;

open (ABUN, "$path/$abun") || die "Cant open $path/$abun\n" ;
my $header = <ABUN> ;
chomp $header ;
my @metn = (split/\t/,$header) ;
shift @metn ;
shift @metn ;
foreach my $met (@metn) {
	unless ($mets{$met}) {
		print "WARNING $met doesnt match\n" ; <STDIN> ;
	}
}

open (OUT, ">normByTaxa.tab") || die "Cant create normByTaxa.tab\n" ;
print OUT "$header\n" ;

while (<ABUN>) {
	chomp ;
	my @fields = (split/\t/,$_) ;
	my $phyla  = shift @fields  ;
	my $matl   = shift @fields  ;
	my $count  = 0 ;
	my $line   = "$phyla\t$matl\t" ;
	foreach my $metab (@fields) {
		my $met  = $metn[$count] ;
		my $norm = $metab / $mets{$met} ;
#		print "$met : $metab -> $norm ($metab / $mets{$met})\n" ; <STDIN> ;
		$line .= "$norm\t" ;
		$count ++ ;
	}
	chop $line ;
	print OUT "$line\n" ; #<STDIN> ;
}
