#install.packages("gplots")
library(gplots)
library(igraph)
library(ggplot2)
library(RColorBrewer)
#library(extrafont)
#font_import()
library(reshape)
library(gridExtra)

rm(list = ls())

setwd("/home/ana/LAB/Llamara/Metagenomes/Annotation/KEGG/unM_NetwkPhotCNSH/")


OTU_table <- read.table("BML_eKOs_FO.tab_1percentRm.tab", header = TRUE, sep = "\t", row.names = 1)

dim(OTU_table)
nsites <- dim(OTU_table)[2]
ntaxa <- dim(OTU_table)[1]


OTU.table.colnames <- colnames(OTU_table)
OTU.table.sparcc   <- cbind(rownames(OTU_table), OTU_table)
MatLayers          <- colnames(OTU_table)
colnames(OTU.table.sparcc) <- c("OTU_id", OTU.table.colnames)

melted <- melt(OTU.table.sparcc)
colnames(melted) <- c("Taxa", "Mat", "Count")

display.brewer.all()
display.brewer.pal(ntaxa, "Set3")
display.brewer.pal(ntaxa, "Set1")
display.brewer.pal(ntaxa, "Spectral")
colores <- colorRampPalette(brewer.pal(ntaxa, "Paired"))

melted$layers <- factor(rep(c(MatLayers), ntaxa), levels = MatLayers)

perMat <- ggplot(melted, aes(x = layers, y = Count, fill = Taxa)) +
  geom_bar(position = "fill", stat = "identity") +
  scale_fill_manual(values = colores(ntaxa)) + 
  xlab("Mat Layers") +
  ylab("Gene Frequency") +
  theme(axis.text.x=element_text(angle=90, hjust=1), legend.position = 'bottom') +
  coord_flip()
perMat
#ggsave("TopLayers_Abundant_Genes.pdf", plot = perMat, width = 12, height = 7, dpi = 300)

colores <- colorRampPalette(brewer.pal(nsites, "Spectral"))
Taxa <- rownames(OTU_table)
melted$taxa <- factor(rep(c(Taxa), nsites), levels = Taxa)

perTaxa <- ggplot(melted, aes(x = taxa, y = Count, fill = Mat)) +
  geom_bar(position = "fill", stat = "identity") +
  scale_fill_manual(values = rev(colores(nsites))) + 
  xlab("") +
  ylab("Counts") +
  theme(axis.text.x=element_text(angle=90, hjust=1)) #, legend.position = 'bottom') +
#  coord_flip()
perTaxa
#ggsave("BML_eKOs_distribution.pdf", plot = perTaxa, width = 12, height = 7, dpi = 300)

sortby = order(rowSums(OTU.table.sparcc[,-1]), decreasing=T)
sortby = order(apply(OTU.table.sparcc[,-1], 1, var), decreasing=F)
sortby = order(apply(OTU.table.sparcc[,-1], 1, function(x){sum(x[1:2])/sum(x)}), decreasing=T)
sortby = order(apply(OTU.table.sparcc[,-1], 1, function(x){abs(diff(x[1:2]))/sum(x[1:2])}), decreasing=F)
matPerTaxa = as.matrix(OTU.table.sparcc[sortby, -1])
matPerTaxa_norm = t(apply(matPerTaxa, 1, function(x){x/sum(x)}))
barplot(t(matPerTaxa_norm), las=2, col=rev(colorRampPalette(brewer.pal(nsites, "Spectral"))(nsites)))

# pdf("TopLayers_Abundance.pdf", width = 12, height = 7)
# par(mar=c(12,5,2,2))
# barplot(as.matrix(t(OTU.table.sparcc[order(rowSums(OTU.table.sparcc[,-1]), decreasing=T),-1])), las=2, col = rev(colorRampPalette(brewer.pal(nsites, "Spectral"))(nsites)))
# dev.off()

#browser()
otus <- t(OTU_table)


###### This piece was taken from https://colab.mpi-bremen.de/micro-b3/svn/EBI2014/Thursday/coocurrences/EBI_co-ocurrence.R
# Let's build the co-ocurrence networks
# First we will need some functions
makeNet <- function(X){
  a<-matrix(nrow=1,ncol=4)
  a[1,1] <- rownames(sparcc$r)[X[1]]
  a[1,2] <- colnames(sparcc$r)[X[2]]
  a[1,3] <- sparcc$r[X[1], X[2]]
  a[1,4] <- sparcc$P[X[1],X[2]]
  return(a)
}

graph.transform.weights <- function (X) {
  require(igraph)
  data.tmp <- matrix(0, nrow=nrow(X), ncol=2)
  dimnames(data.tmp)[[2]] <- c("i", "j")
  data.tmp[,1] <- X[,1]
  data.tmp[,2] <- X[,2]
  data <- data.frame(data.tmp)
  graph.data <- graph.data.frame(data, directed=F)
  E(graph.data)$weight <- abs(as.numeric(X[,3]))
  E(graph.data)$cor <- as.numeric(X[,3])
  E(graph.data)$pvalue <- as.numeric(X[,3])
  summary(graph.data)
  cat("Average degree:",ecount(graph.data)/vcount(graph.data)*2)
  return(graph.data)
}

# Let's read the sparCC correlation matrix
sparcc.cor <- read.table(file = "BML_eKOs_FO_corr.sparcc", sep = "\t", header = T, row.names = 1)
dim(sparcc.cor)


# gudones = names(mean.abu)[mean.abu>minabu]
# f_gudones = names(sparcc.cor) %in% gudones

MAMAtrix = as.matrix(sparcc.cor)

image(MAMAtrix, col=colorRampPalette(c("darkblue", "blue", "lightblue","gray", "pink", "red", "yellow"))(250))
# image(MAMAtrix, col=colorRampPalette(c("white", "red", "darkred","black"), bias=0.5)(250))
# image(MAMAtrix, col=colorRampPalette(c("white", "red", "darkred","black"), bias=1)(250))
# image(MAMAtrix, col=colorRampPalette(c("white", "red", "darkred","black"), bias=1.5)(250))

pdf("BML_eKOsFOx_clusterCorr.pdf", width = 12, height = 7)
heatmap.2(as.matrix(sparcc.cor))
dev.off()
xum = heatmap.2(as.matrix(sparcc.cor))
# checar los objetos aqui dentro que pueden ser utiles.
str(xum)

#browser()
# Let's read the sparCC pseudo p-values
sparcc.pval <- read.table(file = "BML_eKOs_FO_pvals.sparcc", sep = "\t", header = T, row.names = 1)
dim(sparcc.pval)

# We will create a new data structure that contains a list of data frames with the sparcc results
sparcc <- structure(list(r = sparcc.cor, P = sparcc.pval))

# We will filter the correlations magnitudes > 0.3 and with a pseudo p-value < 0.01 and will get the
# coordinates in the dataframe
pval <- 0.001
r <- 0.7
selrp <- which((abs(sparcc$r) > r & sparcc$P < pval) & lower.tri(sparcc$r == TRUE), arr.ind = TRUE)
pdf("BML_clusterPval.pdf", width = 12, height = 7)
heatmap.2(as.matrix(sparcc.pval))
dev.off()

# We use the coordinates to build a dataframe containing the taxon pairs and the correlation coef
# and the pseudo pvalues
sparcc.graph.df <- t(apply(selrp,1, makeNet))

# We transform the dataframe to an igraph object
sparcc.graph <- graph.transform.weights(sparcc.graph.df)

# We will add the distribution information to the nodes
sparcc.graph.names <- as.data.frame(V(sparcc.graph)$name, stringsAsFactors = F)
colnames(sparcc.graph.names) <- "names"


######################################### 21/07/2016 ######################################


# Calculate some node properties and node similarities that will be used to illustrate 
# different plotting abilities

# Calculate degree for all nodes
degAll <- igraph::degree(sparcc.graph, v = V(sparcc.graph), mode = "all")

# Calculate betweenness for all nodes
betAll <- betweenness(sparcc.graph, v = V(sparcc.graph), directed = FALSE) / (((vcount(sparcc.graph) - 1) * (vcount(sparcc.graph)-2)) / 2)
betAll.norm <- (betAll - min(betAll))/(max(betAll) - min(betAll))
rm(betAll)

#Calculate Dice similarities between all pairs of nodes
dsAll <- similarity.dice(sparcc.graph, vids = V(sparcc.graph), mode = "all")

############################################################################################
# Add new node/edge attributes based on the calculated node properties/similarities

sparcc.graph <- set.vertex.attribute(sparcc.graph, "degree", index = V(sparcc.graph), value = degAll)
sparcc.graph <- set.vertex.attribute(sparcc.graph, "betweenness", index = V(sparcc.graph), value = betAll.norm)


## Calculate data from network

summary(sparcc.graph)
transitivity(graph = sparcc.graph, type = "undirected")                  ## Clustering Coefficient
#mean(estimate_betweenness(sparcc.graph, cutoff = 6, normalize = T))     ## Betweenness Centrality
mean(betweenness(sparcc.graph, directed = F, normalized = T))            ## Betweenness Centrality
mean(closeness(sparcc.graph, mode = "all", normalized = T))              ## closeness centrality

vertex_connectivity(sparcc.graph)                                        ## Connectivity === The vertex connectivity of a graph is the minimum vertex connectivity of all (ordered) pairs of vertices in the graph. In other words this is the minimum number of vertices needed to remove to make the graph not strongly connected. (If the graph is not strongly connected then this is zero.) vertex_connectivity calculates this quantitty if neither the source nor target arguments are given. (Ie. they are both NULL.)
decompose.graph(sparcc.graph)
decompose.graph(sparcc.graph)[[1]]
vertex.connectivity(decompose.graph(sparcc.graph)[[1]], checks = T)

edge_density(sparcc.graph, loops = FALSE)
max(degAll)                                                              ## k Max
cliques(sparcc.graph)                                                    ## Number of maximal cliques
mean_distance(sparcc.graph, directed = F)                                ## Shortest Paths Average
diameter(sparcc.graph, directed = F)
mean(edge.betweenness(sparcc.graph, directed = F))


write.graph(sparcc.graph, "BML_eKOsFOx_SparCC.gml", format="gml")

# Add Taxa abundance as a node attribute

orderedAbund <- as.data.frame(rowSums(OTU_table))[names(V(sparcc.graph)),]
assortativity(sparcc.graph, directed = F, types1 = orderedAbund)

sparcc.graph <- set.vertex.attribute(sparcc.graph, "abundance", index = V(sparcc.graph), value = orderedAbund)
summary(sparcc.graph)

write.graph(sparcc.graph, "BML_eKOsFOx_SparCC.graphml", format="graphml")

Filt.Taxa <- OTU.table.sparcc[as.character(OTU.table.sparcc$OTU_id) %in% names(V(sparcc.graph)),]
dim(Filt.Taxa)


FTaxa     <- rownames(Filt.Taxa)
colores   <- colorRampPalette(brewer.pal(nsites, "Spectral"))
#Mcolores <- c("#009933","#59b300","#cc0000","#ff1a1a","#804000", "#663300")
meltedF   <- melt(Filt.Taxa)
colnames(meltedF)  <- c("Filtered.Taxa", "Mat.Layer", "Abundance") 
meltedF$Ftaxa <- factor(rep(c(FTaxa), nsites), levels = FTaxa)

write(as.character(Filt.Taxa$OTU_id), file = "BML_Filtered.txt")

perFTaxa <- ggplot(meltedF, aes(x = reorder(Ftaxa, -Abundance), y = Abundance, fill = Mat.Layer)) +
  geom_bar(position = "fill", stat = "identity") +
  scale_fill_manual(values = rev(colores(nsites))) + 
  xlab("") +
  ylab("Frequency") +
  theme(axis.text.x=element_text(angle=90, hjust=1)) 
  #coord_polar(theta="y")
# theme(legend.position = 'left')
perFTaxa
# ggsave("BML_FiltDisteKOs.pdf", plot = perFTaxa, width = 12, height = 7, dpi = 300)

colores <- colorRampPalette(brewer.pal(ntaxa, "Paired"))
nPFAMs <- length(unique(meltedF$Filtered.Taxa))
perFMat <- ggplot(meltedF, aes(x = Mat.Layer, y = Abundance, fill = Ftaxa)) +
  geom_bar(position = "fill", stat = "identity") +
  scale_fill_manual(values = colores(nPFAMs)) + 
  xlab("") +
  ylab("Frequency") +
  theme(axis.text.x=element_text(angle=90, hjust=1), legend.position = 'bottom') +
  coord_flip()
perFMat
# ggsave("TopLayers_FiltDistMat.pdf", plot = perFMat, width = 12, height = 7, dpi = 300)
# 
# 
# 
# pdf("TopLayers_FilteredAbundance.pdf", width = 12, height = 7)
# par(mar=c(12,5,2,2))
# barplot(as.matrix(t(Filt.Taxa[order(rowSums(Filt.Taxa[,-1]), decreasing=T),-1])), las=2, col = rev(colorRampPalette(brewer.pal(nsites, "Spectral"))(nsites)))
# dev.off()

dtFT <- as.data.frame(t(Filt.Taxa[-c(1)]))
specolors <- as.vector(brewer.pal(nsites, "Spectral"))
NamesTaxa <- colnames(dtFT)
Ntaxa <- dim(dtFT)[2] 


# Use the Abundance to set the node size. 
barplot(table(round(log2(1+V(sparcc.graph)$abundance))))
bins <- as.numeric(as.character(cut(V(sparcc.graph)$abundance, breaks = 8, include.lowest=T, labels = F))) #breaks usually works best in 4
levels(cut(V(sparcc.graph)$abundance, breaks = 4, include.lowest=T))
barplot(table(bins), las =2)
V(sparcc.graph)$size <- bins*5

# The labels are currently node IDs.
# Setting them to NA will render no labels:
V(sparcc.graph)$label.color <- "black"

# Set edge width based on weight:
#E(sparcc.graph)$width <- E(sparcc.graph)$weight*2
#plot(sparcc.graph) 

# Set edge width based on correlation coeficient:
E(sparcc.graph)$width <- E(sparcc.graph)$cor*5


# # Define the Pie Slices
# rebaneidas = lapply(split(Filt.Taxa[V(sparcc.graph)$name,2:(nsites+1)], 1:length(V(sparcc.graph)$name)), unlist)
# names(rebaneidas) = V(sparcc.graph)$name
# 
# set.seed(1)
# #l <- layout.fruchterman.reingold(sparcc.graph) #, repulserad=vcount(sparcc.graph)^3, area=vcount(sparcc.graph)^2.4)
# l <- layout.reingold.tilford(sparcc.graph, circular = T) 
# colores   <- colorRampPalette(brewer.pal(nsites, "Spectral"))
# AbundanceLevels <- levels(cut(V(sparcc.graph)$abundance, breaks = 4, include.lowest=T))
# 
l <- layout.auto
# 
# par(mar=c(2,3,2,2)+0.1)
# pdf("TopLayerNetwork_Auto.pdf", width = 12, height = 7)
# plot(sparcc.graph, layout = l, 
#      vertex.shape="pie", vertex.pie = rebaneidas, #vertex.label.family="Arial Black",
#      vertex.pie.color = list(colores(nsites)), vertex.pie.border = "#00000000",
#      edge.curved = .1)
# legend(x=1.1, y=-0.8, AbundanceLevels, pch=21,
#        col="#777777", pt.bg = rep("#8c8c8c", length(unique(bins))), pt.cex=c(1,2,3,5), bty="n", ncol=1)
# legend(x=1.4, y=1.3, c(MatLayers), pch=21,
#        col="#777777", pt.bg = colores(nsites), pt.cex=2.5, bty="n", ncol=1)
# dev.off()



# write.graph(sparcc.graph, "TopLayers_SparCC.graphml", format="graphml")








l <- layout.auto




# par(mar=c(2,3,2,2)+0.1)
# pdf("BML_eKOs_Network.pdf", width = 12, height = 7)

KOinfo <- read.table("KOs_pathway.list.txt", header = T, sep = "\t") #, row.names = 1)
colnames(KOinfo) <- c("Metabolism", "KO")
V(sparcc.graph)$Metab=as.character(KOinfo$Metabolism[match(V(sparcc.graph)$name,KOinfo$KO)]) 
write.graph(sparcc.graph, "BML_eKOsFOx_SparCC.graphml", format="graphml")











# V(sparcc.graph)$color=V(sparcc.graph)$Metab #assign the "Metabolism" attribute as the vertex color
# V(sparcc.graph)$color=gsub("B","purple",V(sparcc.graph)$color) #Chromatin structure and dynamics
# V(sparcc.graph)$color=gsub("C","yellow",V(sparcc.graph)$color) #Energy production and conversion
# V(sparcc.graph)$color=gsub("E","coral3",V(sparcc.graph)$color) #Amino acid transport and metabolism
# V(sparcc.graph)$color=gsub("F","cornflowerblue",V(sparcc.graph)$color) #Nucleotide transport and metabolism
# V(sparcc.graph)$color=gsub("G","royalblue",V(sparcc.graph)$color) #Carbohydrate transport and metabolism
# V(sparcc.graph)$color=gsub("H","lightseagreen",V(sparcc.graph)$color) #Coenzyme transport and metabolism
# V(sparcc.graph)$color=gsub("I","deeppink",V(sparcc.graph)$color) #Lipid transport and metabolism
# V(sparcc.graph)$color=gsub("K","brown",V(sparcc.graph)$color) #Transcription
# V(sparcc.graph)$color=gsub("M","darkgoldenrod3",V(sparcc.graph)$color) #Cell wall/membrane/envelope biogenesis
# V(sparcc.graph)$color=gsub("O","deepskyblue4",V(sparcc.graph)$color) #Post-translational modification, protein turnover, and chaperones
# #V(sparcc.graph)$color=gsub("P","firebrick",V(sparcc.graph)$color) #Inorganic ion transport and metabolism
# V(sparcc.graph)$color=gsub("P","purple",V(sparcc.graph)$color) #Inorganic ion transport and metabolism
# V(sparcc.graph)$color=gsub("Q","forestgreen",V(sparcc.graph)$color) #Secondary metabolites biosynthesis, transport and metabolism
# V(sparcc.graph)$color=gsub("T","darksalmon",V(sparcc.graph)$color) #Signal transduction mechanisms
# V(sparcc.graph)$color=gsub("U","darkslategray",V(sparcc.graph)$color) #Intracellular trafficking, secretion, and vesicular transport
# 
# 
# plot(sparcc.graph, layout = l, vertex.shape="circle", edge.curved = .1)
# legend(x=1.1, y=-0.8, AbundanceLevels, pch=21,
#        col="#777777", pt.bg = rep("#8c8c8c", length(unique(bins))), pt.cex=c(1,2,3,5), bty="n", ncol=1)
# legend(x=1.4, y=1.3, unique(as.character(COGinfo$Metabolism[match(V(sparcc.graph)$name,COGinfo$COG)])), pch=21,
#        col="#777777", pt.bg = c("yellow", "purple","lightseagreen", "coral3"), pt.cex=2.5, bty="n", ncol=1)
# 
# dev.off()



