**Raw files**

LLA9-A1.II_R1.fastq.bz2    
LLA9-A1.II_R2.fastq.bz2   
LLA9-B1.I_f.slx.bz2         
LLA9-B1.I_r.slx.bz2         
LLA9-B2.I_f.slx.bz2        
LLA9-B2.I_r.slx.bz2        
LLA9-C1.II_R1.fastq.bz2     
LLA9-C1.II_R2.fastq.bz2     
LLA9-C2.III_R1.fastq.bz2
LLA9-C2.III_R2.fastq.bz2
LLA9-C2.II_R2.fastq.bz2
LLA9-C3.III_R1.fastq.bz2
LLA9-C3.III_R2.fastq.bz2
LLA9-C3.II_R1.fastq.bz2
LLA9-C3.II_R2.fastq.bz2
LLA9-D1.I_f.slx.bz2
LLA9-D1.III_R1.fastq.bz2
LLA9-D1.III_R2.fastq.bz2
LLA9-D1.I_r.slx.bz2
LLA9-D2.I_f.slx.bz2
LLA9-D2.III_R1.fastq.bz2
LLA9-D2.III_R2.fastq.bz2
LLA9-D2.I_r.slx.bz2


**Pipeline used**

**FastQC version 0.11.4** was used to check the quality of the above mentioned illumina reads.

**Trimmomatic version 0.36** was used to clean the reads with the following parameters:
ILLUMINACLIP:adapters.faa:2:30:10; LEADING:3; TRAILING:3; MAXINFO:40:0.8 and MINLEN:36. 
*adapters.faa* file was generated based on the FastQC report.

**EMIRGE version 0.61** was used to extract the 16S genes directly from the cleaned reads by mapping them to the SILVA database.

**Nonpareil version 2.4** was used to estimate the sequencing coverage effort. 
The plots were generated with *ad hoc* R script *nonpareil.R* using the **nonpareil package version 3.3** for R.

**Megahit version 1.3** was used to assembly the reads into contigs with default parameters but a minimum length of 200 bp for the assembled contigs 
and a starting kmer size of 23 up to 93 with an increasing step of 10.

The reads were mapped to the assemblies to calculate the coverage of each assembly using **Bowtie2 version 2.2.6** and **SAMtools version 1.6**
GC content of the assemblies was calculated with **Multimetagenome** script *calc.genome.stats* and they were plotted with an *ad hoc* R sript *GC.R*

**Prokka version 1.12** was used to predict all the genes in the newly assembled contigs.

**Diamond version 0.7.9.58** was used on all the predicted genes to a *Custom+RefSeq Database* to match genes in the NCBI database.
For each of the matched genes to the NCBI database, an *ad hoc* perl script *fetchTaxID.pl* was used to retrieve their taxID via NCBI’s e-Fetch.

COGs were assigned by profile hidden Markov model (profile HMM) searches using the *hmmsearch* program of the **HMMER3 package version 3.1b1**. 
For every COG, a multiple sequence alignment of bona fide representative sequences were generated using the **Muscle program version 3.8.31** and then, 
the corresponding Hidden Markov Model was built using the *hmmerbuild* program, also provided in the **HMMER3 package**
PFAMs (Pfam-A) were predicted with *hmmersearch* tool from **HMMER3 version 3.1b1**.
KEGG orthologs (KOs) were assigned via **GhostKOALA web server**.

All the stacked bars graphics and plots were constructed with *ad hoc* scripts in R using the **ggplot2 version 2.2.1 package** 
or the **plotly version 4.7.1 package**.

CCAs were calculated using an *ad hoc* script in R with the aid of the **Vegan package version 2.0-10**. 
Heatmaps were also constructed with an R script.

To be able to compare gene abundances inter- and intra-metagenomes the software **MUSSiC** was used via its web server.
*Ad hoc* perl scripts were written to generate matrices of the abundance of all the predicted COGs, KOs and PFAMs. 
This script also reduced the matrix by eliminating low abundant genes.
**SparCC (scripted in Jun 20, 2011)** was used to calculate the correlation and p-values matrices of the genes co-occurring in the metagenomes. 
Ten iterations were used to estimate the median correlation of each pair and the statistical significance of the correlations was calculated by 
bootstrapping with 500 iterations. Correlations were then sorted according to their statistical significance; we retained only those with 
*p* < 0.001 and R > 0.7 or R < -0.7. 
Networks were built using *ad hoc* scripts in R and visualized with the aid of the **igraph package version 1.0.1** (http://igraph.org/);
and were visualized with **Cytoscape version 3.6.1**


